# TranslationNotificationStats

Script to output the number of notifications and number of unique users that sent those notifications over a period of `n` months. There's 

## To get results
1. Run `node chunked.js`
2. You may adjust the number of months, `n`  found on the variable `intervalMonths` to chunk the results.

### Example

```
From 2012-05-09 to 2013-03-09
Number of notifications: 86
Number of unique users: 8
---
Interval 2:
From 2013-03-09 to 2014-01-09
Number of notifications: 67
Number of unique users: 15
---
Interval 3:
From 2014-01-09 to 2014-11-09
Number of notifications: 4
```
