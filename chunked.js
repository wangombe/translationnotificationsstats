const fs = require('fs');
const path = require('path');

// Load JSON data
const logs = {
	filename: 'meta.wikimedia.org_logevents.csv',
	data: JSON.parse(fs.readFileSync('meta.wikimedia.org_logevents.json', 'utf8')).query.logevents
};
// const logs = {
// 	filename: 'mediawiki.org_logevents.csv',
// 	data: JSON.parse(fs.readFileSync('mediawiki.org_logevents.json', 'utf8')).query.logevents
// };
// const logs = {
// 	filename: 'commons.wikimedia.org_logevents.csv',
// 	data: JSON.parse(fs.readFileSync('commons.wikipedia.org_logevents.json', 'utf8')).query.logevents
// }

// Function to parse the timestamp into a Date object
const parseTimestamp = timestamp => new Date(timestamp);

// Function to add months to a date
const addMonths = (date, months) => {
    const newDate = new Date(date);
    newDate.setMonth(newDate.getMonth() + months);
    return newDate;
};

// Define the start date and the interval
// The last item in the data is the first entry in the logs
let firstItemTimestamp = logs.data[logs.data.length - 1].timestamp

// Define the start date and the interval
const startDate = new Date(firstItemTimestamp);
const intervalMonths = 2;

// Initialize arrays to store trends
let trendData = [];

// Calculate the end date of the data
const endDate = new Date(Math.max(...logs.data.map(log => parseTimestamp(log.timestamp))));

// Function to calculate statistics for a given period
const calculateStatistics = (start, end) => {
    let notifications = 0;
    let users = new Set();

    logs.data.forEach(log => {
        const logDate = parseTimestamp(log.timestamp);
        if (logDate >= start && logDate < end) {
            notifications++;
            users.add(log.user);
        }
    });

    return { notifications, uniqueUsers: users.size };
};

// Calculate statistics for each interval
let currentStartDate = startDate;
while (currentStartDate < endDate) {
    const currentEndDate = addMonths(currentStartDate, intervalMonths);
    const stats = calculateStatistics(currentStartDate, currentEndDate);
    trendData.push({
        startDate: currentStartDate.toISOString().split('T')[0],
        endDate: currentEndDate.toISOString().split('T')[0],
        ...stats
    });
    currentStartDate = currentEndDate;
}

// Create CSV content
let csvContent = 'Interval,Start Date,End Date,Number of Notifications,Number of Unique Users\n';
trendData.forEach((interval, index) => {
    csvContent += `${index + 1},${interval.startDate},${interval.endDate},${interval.notifications},${interval.uniqueUsers}\n`;
});

// Write to CSV file
const outputPath = path.join(__dirname, logs.filename);
fs.writeFileSync(outputPath, csvContent);

console.log(`Data has been written to ${outputPath}`);
